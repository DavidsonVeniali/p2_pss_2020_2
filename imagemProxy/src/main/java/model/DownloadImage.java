/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author Davidson
 */
public class DownloadImage {

    public ImageIcon pegandoImagem(String url, int heigth, int width) throws IOException {
        try {
            URL imageUrl = new URL(url);
            InputStream in = imageUrl.openStream();
            BufferedImage image = ImageIO.read(in);
            in.close();
            ImageIcon imageIcon = new ImageIcon(image.getScaledInstance(heigth, width, Image.SCALE_DEFAULT));
            return imageIcon;
        } catch (IOException ioe) {
            throw new IOException("Não foi possível localizar a URL");
        }
    }

}
