/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.IOException;
import javax.swing.ImageIcon;

/**
 *
 * @author Davidson
 */
public class RealImageModel implements ImageInterfaceModel{
    private final String fileName;
    private ImageIcon imagemAltaResolucao;

    public String getFileName() {
        return fileName;
    }
    
    public RealImageModel(String fileName, String url) throws IOException{
        this.fileName = fileName;
        loadFromDisk(url);
    }
    
    @Override
    public ImageIcon display(){
        return imagemAltaResolucao;
    }
    
    private void loadFromDisk(String url) throws IOException{
        this.imagemAltaResolucao = new DownloadImage().pegandoImagem(url, 600, 600);
    }
}
