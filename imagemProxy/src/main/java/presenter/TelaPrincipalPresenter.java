/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presenter;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.DownloadImage;
import model.ProxyImageModel;
import view.TelaPrincipal;

/**
 *
 * @author Davidson
 */
public class TelaPrincipalPresenter {

    private TelaPrincipal tela;
    private static TelaPrincipalPresenter instance;
    ArrayList<ProxyImageModel> imagemCollection = new ArrayList<>();

    public static TelaPrincipalPresenter getInstance() {
        if (instance == null) {
            instance = new TelaPrincipalPresenter();
        }
        return instance;
    }

    public TelaPrincipal getTela() {
        return tela;
    }

    private TelaPrincipalPresenter() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        tela = new TelaPrincipal();
        tela.setVisible(true);

        try {
            tela.getLblImagem1().setIcon(new DownloadImage().pegandoImagem("https://gitlab.com/DavidsonVeniali/imagensp2/-/raw/master/cidades_640.jpg", 140, 140));
            tela.getLblImagem2().setIcon(new DownloadImage().pegandoImagem("https://gitlab.com/DavidsonVeniali/imagensp2/-/raw/master/cidade2_640.jpg", 140, 140));
            tela.getLblImagem3().setIcon(new DownloadImage().pegandoImagem("https://gitlab.com/DavidsonVeniali/imagensp2/-/raw/master/cidade3_640.jpg", 140, 140));
            tela.getLblImagem4().setIcon(new DownloadImage().pegandoImagem("https://gitlab.com/DavidsonVeniali/imagensp2/-/raw/master/cidade4_640.jpg", 140, 140));
            tela.getLblImagem5().setIcon(new DownloadImage().pegandoImagem("https://gitlab.com/DavidsonVeniali/imagensp2/-/raw/master/cidade5_640.jpg", 140, 140));

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(tela, ex.getMessage());
        }

        iniciaListener(tela);

    }

    public void iniciaListener(TelaPrincipal tela) {
        tela.getLblImagem1().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                ProxyImageModel proxy = verificaProxy("imagem1.jpg");
                if (proxy == null) {
                    proxy = new ProxyImageModel("imagem1.jpg", "https://gitlab.com/DavidsonVeniali/imagensp2/-/raw/master/cidades_1920.jpg");
                    imagemCollection.add(proxy);
                }
                    tela.getLblImagemAltaResolucao().setIcon(proxy.display());
                } catch (IOException ex) {
                    Logger.getLogger(TelaPrincipalPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        tela.getLblImagem2().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                ProxyImageModel proxy = verificaProxy("imagem2.jpg");
                if (proxy == null) {
                    proxy = new ProxyImageModel("imagem2.jpg", "https://gitlab.com/DavidsonVeniali/imagensp2/-/raw/master/cidade2_1920.jpg");
                    imagemCollection.add(proxy);
                }

                    tela.getLblImagemAltaResolucao().setIcon(proxy.display());
                } catch (IOException ex) {
                    Logger.getLogger(TelaPrincipalPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        tela.getLblImagem3().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    ProxyImageModel proxy = verificaProxy("imagem3.jpg");
                    if (proxy == null) {
                        proxy = new ProxyImageModel("imagem3.jpg", "https://gitlab.com/DavidsonVeniali/imagensp2/-/raw/master/cidade3_1920.jpg");
                        imagemCollection.add(proxy);
                    }
                    tela.getLblImagemAltaResolucao().setIcon(proxy.display());
                } catch (IOException ex) {
                    Logger.getLogger(TelaPrincipalPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        tela.getLblImagem4().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    ProxyImageModel proxy = verificaProxy("imagem4.jpg");
                    if (proxy == null) {
                        proxy = new ProxyImageModel("imagem4.jpg", "https://gitlab.com/DavidsonVeniali/imagensp2/-/raw/master/cidade4_1920.jpg");
                        imagemCollection.add(proxy);
                    }
                    tela.getLblImagemAltaResolucao().setIcon(proxy.display());
                } catch (IOException ex) {
                    Logger.getLogger(TelaPrincipalPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        tela.getLblImagem5().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    ProxyImageModel proxy = verificaProxy("imagem5.jpg");
                    if (proxy == null) {
                        proxy = new ProxyImageModel("imagem5.jpg", "https://gitlab.com/DavidsonVeniali/imagensp2/-/raw/master/cidade5_1920.jpg");
                        imagemCollection.add(proxy);
                    }

                    tela.getLblImagemAltaResolucao().setIcon(proxy.display());
                } catch (IOException ex) {
                    Logger.getLogger(TelaPrincipalPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
    }

    private ProxyImageModel verificaProxy(String nome) {
        for (ProxyImageModel imagemModel : imagemCollection) {
            if (imagemModel.getFileName().equals(nome)) {
                return imagemModel;
            }
        }
        return null;
    }
}
